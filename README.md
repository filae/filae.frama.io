Bienvenue dans le dépôt de documentation de FILAÉ pour la planification, la rédaction et la communication des certificats de ses consultations médico-judiciaires. Ce dépôt a pour objectif de contenir toutes les ressources nécessaires à la découverte, la mise en œuvre et l'utilisation de cette solution logicielle.

## Aspects fonctionnels

Ce logiciel n'outille pas un mode de fonctionnement particulier, mais vous permet de paramétrer le vôtre.

### Agenda

Chaque prise de rendez-vous est associée à un _motif de consultation_ et planifiée sur un des _créneaux d’intervention_ définis pour chaque _domaine d’intervention_. Quelques exemples :

* Domaine d’intervention : victimologie, antenne mobile, etc.
* Créneau d’intervention : du lundi au vendredi de 9h à 18h sur le site D., du vendredi 18h au lundi 9h en déplacement, etc.
* Consultation : le jeudi 9 février 2017 de 14h30 à 15h00 sur le site D., le dimanche 13 février 2017 à 3h45 au commissariat de V., etc.
* Motif de consultation : AS, violences volontaires, accident sur la voie publique, etc.

Chaque utilisateur ayant droit de gérer l’agenda peut consulter la liste des rendez-vous dans un semainier. Ce semainier est par défaut centré sur le jour courant, et l’utilisateur peut aisément naviguer dans le passé et dans le futur pour revenir sur ou planifier un rendez-vous. Toutes les informations requises à la prise de rendez-vous sont saisies par l’utilisateur, avec de nombreuses aides à la saisie : recherche de patient et remplissage automatique avec les données connues, menus déroulant pour les motifs de consultation, indicateur visuel des créneaux d’intervention…

### Certificats

Cette plateforme logicielle permet de produire des certificats de consultation médico-judiciaire. Ces certificats sont saisis grâce à un modèle de certificat propre à chaque motif de consultation.

Une interface de gestion des modèles de certificat permet de faire évoluer le questionnaire associé à un motif de consultation. Au début de chaque rendez-vous, le dernier modèle validé pour ce motif de consultation est utilisé pour la saisie du certificat par le médecin. Toutes modifications ultérieures du modèle ne sauraient être appliquées à un certificat de consultation déjà amorcé.

Les informations sont sauvegardées au fur et à mesure de leur saisie. À la fin du rendez-vous, un aperçu du certificat peut être consulté par le médecin avant sa validation et l’envoi possible à l’entité requérante. L’intégrité des données du certificat est garantie par l’utilisation de sommes de contrôle et l’utilisation du format PDF, spécialement adapté au stockage de document.

Toute modification du certificat est alors interdite via les API, et _a fortiori_ via l’interface utilisateur, et une modification fallacieuse par d’autres biais (accès direct à la base ou au système de fichier, notamment) pourra être détectée.

L’accès _a posteriori_ aux dossiers d’examen peut se faire par sa fiche-patient, son rendez-vous de consultation, ou par l’interface de recherche intégrée à l'application.

## Aspects techniques

Cette solution logicielle prend la forme d'une constellation de plusieurs projets, aux responsabilités bien déterminées, aux interactions nombreuses, accessibles dans une interface utilisateur unifiée.

### Interface utilisateur

Le projet `web-gui` est une [_single-page application_](https://en.wikipedia.org/wiki/Single-page_application) écrite en [EmberJS](https://www.emberjs.com/), une bibliothèque JavaScript à destination de nos navigateurs.

Ce projet est la partie émergée de l'iceberg. Il offre à l'utilisateur l'accès à toutes les informations pour lesquelles il est habilité, et les interactions possibles sur celles-ci. Toutes les données et interactions sont issues et à destination de la constellation côté serveur.

### Serveur d'API

Le projet `web-api` est une application [_Rails API-only_](https://guides.rubyonrails.org/api_app.html), interlocuteur principal pour l'interface utilisateur.

Il met à disposition des points d'entrée HTTP pour récupérer et agir sur les données dans le système. Il orchestre toutes ces opérations conformément aux besoins métier et à la réglementation en vigueur. Il interagit alors avec les différentes bases de données afin de construire ou présenter une information consolidée et cohérente.

#### Base de données relationnelle

Le serveur d'API communique avec une base de données [PostgreSQL](https://www.postgresql.org/). Cette base de données renferme toutes les informations intrinsèquement relationnelles, notamment la configuration du mode de fonctionnement du service (domaines d'intervention, motifs de consultation, etc.), la gestion des utilisateurs et des habilitations…

#### Base de données orientée documents

Le serveur d'API profite également des capacités "orientées documents" de PostgreSQL pour le stockage de documents JSON, version expérimentale des certificats et de leurs modèles.

## Organisation de ce dépôt

Vous trouverez ici plusieurs sous-dossiers pour différents aspects de documentation :

1. `website` pour le site public permettant de découvrir FILAÉ en quelques pages.
