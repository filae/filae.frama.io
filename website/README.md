# Site de présentation de FILAÉ

Pour lancer le serveur :

0. Installez la bonne version de ruby : `rbenv install` ;
0. Installez les dépendances : `bundle install` ;
0. Démarrez Jekyll : `bundle exec jekyll serve`.

Rajoutez l'option `--drafts` à la fin de cette dernière ligne de commande pour prendre en compte les brouillons d'articles de blog.

## Conception et présentation de diagramme

Le greffon `ember-spaceship` permet d'intégrer différents types de diagramme dans les pages et articles de ce site. Le rendu est le meilleur avec la variante `plantuml`. Cependant, l'intégration manque de lisibilité ou déborde de la page. Ainsi, une conversion en image puis galerie [`prettyPhoto`](https://github.com/scaron/prettyphoto) est recommandée dans la plupart des cas :

1. conversion en image :
   1. commenter la propriétét `display: none` de la classe `.plantuml` dans `_base.scss` ;
   1. cela affiche le SVG généré par `ember-spaceship` ;
   1. un clic-secondaire sur le SVG puis `Inspect` ;
   1. dans l'inspecteur, clic-secondaire sur le nœud `svg` puis `Screenshot node` ;
   1. déplacer le fichier image généré dans le dossier `images` de ce projet ;
   1. décommenter la propriété `display: none` de la classe `.plantuml` dans `_base.scss` ;
1. galerie `prettyPhoto` :
   1. voir par exemple dans `gros-plan-certificats-medicaux.md`.

## Réalisation des captures d'écran

1. Lancer l'application Web au couleur de ce site ;
   1. Dans le fichier `docker-compose.yml` de Filaé, modifier la variable d'environnement `TOOLBAR_COLOR` en `006666` ;
   1. Il s'agit de la même couleur principale que ce site, cf `$brand-color` dans `main.scss` ;
   1. Lancer l'application ;
1. Configurer une taille de capture "plein écran" homogène ;
   1. Activer la vue adaptative `Maj+Ctrl+M` ;
   1. Sélectionner dans le menu déroulant des périphériques `720p HD Television` ;
      1. Il vous faudra peut-être l'activer dans le menu `Edit list…` de ce même menu ;
      1. Sinon, vous pouvez saisir manuellement une largeur de `1280` et une hauteur de `720`.
   1. Vous pouvez maintenant faire des captures avec `Maj+Ctrl+S`.
