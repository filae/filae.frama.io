(function ($) {

  var contact_us_blink = function () {
    var contact_us_info = $('.site-footer');

    var contact_us_links = $("a[href=#contact-us]")
    contact_us_links.click(function (event) {
      var toggleRounds = 2
      var toggledClass = 'highlight'

      for (var i = 0; i < toggleRounds; i++) {
        setTimeout(function () {
          contact_us_info.toggleClass(toggledClass)
        }, i * 500)
      }
    })
  }

  var external_links_hint = function () {
    var external_links = $("a[href^=http]")

    external_links.addClass('external')
  }

  var prettyPhoto = function () {
    $("a[rel^='prettyPhoto']").prettyPhoto({social_tools: ''});
  }

  var prettyPhotoResize = function () {
    var picts = $("a[rel^='prettyPhoto'].prettyPhotoResize")

    var count_by_gallery = {}
    picts.each(function () {
      var gallery = $(this).attr('rel');
      count_by_gallery[gallery] = (count_by_gallery[gallery] || 0) + 1;
    });

    for (var key in count_by_gallery) {
      if (count_by_gallery.hasOwnProperty(key)) {
        pict_count = count_by_gallery[key]
        $("a[rel='"+key+"'] img").css('max-width', 95 / pict_count + "%")
      }
    }
  }

  $(contact_us_blink)
  $(external_links_hint)
  $(prettyPhoto)
  $(prettyPhotoResize)
})(jQuery)
