---
layout: default
title: "Ambitions"
permalink: /
nav: header
---
{::options parse_block_html="true" /}

<div class="main-content">

# Accueil
{: .display-off}

## Ambitions
{: #ambitions .display-off}

<span>Facilitons la circulation du dossier-patient sécurisé,</span>
<span>en reliant les informations</span>
<span>administratives et médicales</span>
<span>sur un support numérique.</span>
{: #vision .centered}

Envie que l'outil informatique soit remis
au service des interactions humaines ?
<a href="#contact-us" title="Lien vers nos coordonnées">Discutons&nbsp;<i class="fa fa-paper-plane-o"></i></a>
{: #impact }

## Organisation
{: .display-off}

<div class="interventions-details">
<div class="intervention">
### <i class="fa fa-hospital-o"></i> Des besoins issus du quotidien.

Amorcé à la demande de l'<abbr title='Unité Médico-Judiciaire des Yvelines'>UMJ78</abbr>, à partir de ses besoins spécifiques, cet outil facilite leur quotidien depuis septembre 2018.

Chaque remarque des utilisateurs de FILAÉ est **prise en considération**, car ce sont eux qui l'éprouvent à longueur de journée.
</div>

<div class="intervention">
### <i class="fa fa-users"></i> Une orientation collégiale.

Conçu dans une optique d'essaimage, rien n'est gravé dans le marbre. Pour une même mission, plusieurs modes de fonctionnement peuvent cohabiter et se compléter.

Comme tout logiciel libre, le futur de FILAÉ dépend de **la communauté qui l'anime**.
</div>

<div class="intervention">
### <i class="fa fa-cogs"></i> Des évolutions au fil de l'eau.

Notre gestion de projet agile permet de déployer des mises à jour fréquentes et fiables. En tant qu'application Web, cela se fait même sans intervention sur vos périphériques.

**Collaborons** de façon à apporter  à tous un maximum de valeur, le plus tôt possible. **Et itérons**.
</div>
</div>

## Aperçu
{: #propositions .display-off}

<figure class="offer-col">
<a href="{% link images/captures/agenda.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/captures/agenda.png %}" alt="Faites vivre vos agendas de consultation partagés : nouveau rendez-vous, annulation/report, prise en charge… Avec codes couleur et pictogrammes pour tout survoler en un coup d'œil." />
</a>
<figcaption>
Faites vivre vos agendas de consultation partagés : nouveau rendez-vous, annulation/report, prise en charge… Avec codes couleur et pictogrammes pour tout survoler en un coup d'œil.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/captures/edition-certificat.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/captures/edition-certificat.png %}" alt="Vous avez pris en charge une consultation et téléversé le certificat rédigé ? Prévisualisez le certificat avant validation et enrichissez le dossier de documents à caractère médical ou non." />
</a>
<figcaption>
Vous avez pris en charge une consultation et téléversé le certificat rédigé ? Prévisualisez le certificat avant validation et enrichissez le dossier de documents à caractère médical ou non.
</figcaption>
</figure>

<div class="offer-col-wrapper">
<figure class="offer-col">
<a href="{% link images/captures/flux.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/captures/flux.png %}" alt="Sachez toujours où vous en êtes grâce à la file d'attente de consultations en antenne mobile, disponible sur votre périphérique mobile, même au fin fond de votre département." />
</a>
<figcaption>
Sachez toujours où vous en êtes grâce à la file d'attente de consultations en antenne mobile, disponible sur votre périphérique mobile, même au fin fond de votre département.
</figcaption>
</figure>


<figure class="offer-col">
<a href="{% link images/captures/admin-modeles.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/captures/admin-modeles.png %}" alt="Rassemblez vos modèles de certificat en un référentiel unique disponible dans l'application. Proposés à la prise en charge d'une consultation, les médecins sont sûrs d'avoir toujours la bonne version." />
</a>
<figcaption>
Rassemblez vos modèles de certificat en un référentiel unique disponible dans l'application. Proposés à la prise en charge d'une consultation, les médecins sont sûrs d'avoir toujours la bonne version.
</figcaption>
</figure>
</div>

Afin d'appréhender ce qui se cache concrètement derrière ces derniers paragraphes, quoi de mieux qu'un petit aperçu des [fonctionnalités de FILAÉ]({% link fonctionnalites/index.md %}).
{: .follow-up }

</div>
