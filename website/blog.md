---
layout: page
title: Blog
permalink: /blog/
nav: header
order: 4
---

<p class="rss-subscribe">S'abonner <a href="{% link feed.xml %}">via RSS <i class="fa fa-rss"></i></a></p>

<ul class="post-list">
{% for post in site.posts %}
	<li>
		<h2>
			<a class="post-link" href="{{ post.url | relative_url }}">{{ post.title }}</a>
			<span class="post-meta">{{ post.date | date: "%d/%m/%Y" }}</span>
		</h2>
		<p>{{ post.excerpt }}</p>
	</li>
{% endfor %}
</ul>
