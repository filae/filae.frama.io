---
layout: post
title:  "Où sont hébergées vos données ?"
categories: blog
---

Sans grand suspense, la réponse est "chez vous". Pour tous les acteurs du projet, la sécurité et la souveraineté des données traitées par Filaé est cruciale. Ainsi, elles ne peuvent être confiées qu'à un tiers qui a toute votre confiance (et celle de nos institutions). Ceci dit, la définition de "chez vous" reste assez large, alors nous allons tenter de détailler tout cela.

## Là où les données vivent

Pour commencer, petit point architecture logicielle : Filaé est une application Web qui se compose d'une part d'une application JavaScript utilisable depuis la plupart des navigateurs Internet dits "modernes", et d'autre part d'une application Ruby qui va recueillir et traiter toutes les demandes envoyées par l'utilisateur.

<figure class="offer-col">
<a href="{% link images/blog/circulation-donnees.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/blog/circulation-donnees.png %}" alt="Schéma de circulation des données et interactions humaines." />
</a>
</figure>

C'est cette application Ruby, côté serveur, qui endosse l'essentiel des responsabilités envers les données. _In fine_, elle les protège des regards indiscrets, assure leur cohérence et facilite leur sauvegarde. Bien choisir là où cette application Ruby sera installée est une question de première importance, et deux options s'offrent à vous.

### Au sein du SI de l'hôpital

Chaque hôpital gère son parc informatique, aussi bien les postes de travail que les imprimantes, mais également quantité de serveurs où sont stockées les données saisies ou collectées grâce aux applications que vous utilisez au quotidien. La Direction des Systèmes d'Information peut administrer cela en interne, ou sous-traiter tout ou partie de cette problématique à un infogérant.

### Chez un hébergeur certifié HDS

L'Agence du Numérique en Santé certifie et maintient à jour une liste d'hébergeurs satisfaisant aux contraintes de protection des données médicales à caractère personnel. Cette option est intéressante pour les hôpitaux qui souhaitent déléguer toutes les compétences d'installation et de maintien en conditions opérationnelles de l'infrastructure matérielle, réseau et système de l'hôpital.

## Et pour Filaé alors ?

Cela n'a aucune importance. Tant que ce serveur est accessible depuis Internet, et a accès à Internet, tout roule.

Tant que des sauvegardes des fichiers et, le cas échéant, de la machine complète sont régulières et la restauration fonctionnelle, tout roule.

Tant que le choix est fait de manière consciente et éclairée pour éviter les indisponibilités et/ou les fuites de données de santé hors de la juridiction française et européenne, tout roule.

Tant que les serveurs sont maintenus à jour afin de colmater les failles de sécurité découvertes et corrigées très régulièrement, tout roule.

Tant que nous parlons de serveurs exécutant comme système d'exploitation une version maintenue de GNU/Linux, tout roule.

## Un exemple de contractualisation vertueuse

La <abbr title="Direction des Systèmes d'Information">DSI</abbr> de l'hôpital met à disposition des machines virtuelles au sein de leur parc de serveurs où le prestataire de maintenance à toute latitude en matière d'administration système : mise à jour du système d'exploitation, gestion des dépendances, supervision logicielle… Un contrat de maintenance pluri-annuel facturé au trimestre permet de financer le maintien en condition opérationnel de la solution et le déploiement des nouvelles versions du logiciel.

La <abbr title="Direction des Systèmes d'Information">DSI</abbr> a la charge, elle, de la disponibilité des machines (allumées, intègres et accessibles par le réseau) et des différentes stratégies de sauvegarde (instantanés à chaud, fichiers, bases de données), conformément au plan de reprise d'activité de l'hôpital.

Cette séparation des responsabilités permet au prestataire de développement logiciel de jouir d'une grande liberté dans les évolutions du socle technique de la solution logicielle quand l'hôpital conserve la maîtrise de ses infrastructures matérielles et de sécurité.
