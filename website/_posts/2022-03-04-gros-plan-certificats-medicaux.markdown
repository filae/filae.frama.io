---
layout: post
title:  "Gros plan sur les certificats médicaux"
categories: blog
---

Nous avons vu dans de précédents articles comment Filaé contribue à une gestion efficace des demandes de consultation, que ce soit [sur rendez-vous]({% post_url 2021-05-07-gros-plan-sur-les-agendas-partages %}) ou [en flux]({% post_url 2021-05-10-gros-plan-sur-les-flux-de-consultations %}). Mais ce n'est qu'une étape intermédiaire vers la délivrance de certificats médicaux, l'objectif premier des UMJ.

{::options parse_block_html="true" /}
<div class="main-content">

## Les certificats téléversés

Ce premier cycle de vie a été développé pour accélérer la transition vers Filaé. Il s'agit d'un mode intermédiaire entre la gestion manuelle historique et le zéro-papier.

```plantuml!
participant Ordinateur
participant CertificatSigné
participant CertificatValidé
participant OPJ
participant CertificatEnvoyé

note over Ordinateur: Modèle Texte
Ordinateur->>Ordinateur: [Praticien] Rédaction
note over Ordinateur: Document Texte
Ordinateur->>Ordinateur: [Praticien] Impression et signature manuscrite
note over Ordinateur: Document Papier
Ordinateur->>Ordinateur: [Praticien] Numérisation
note over Ordinateur : Document PDF
Ordinateur->>CertificatSigné: [Secrétaire] Téléversement
note right of CertificatSigné: Document PDF
CertificatSigné->>CertificatValidé: [Secrétaire] Clic
note right of CertificatValidé: Document PDF + Sceau
CertificatValidé->>OPJ: [Secrétaire] Clic
note right of OPJ: Document PDF + Sceau
CertificatValidé->>CertificatEnvoyé: [Automatique] Horodatage
note right of CertificatEnvoyé: Document PDF + Sceau
```

<figure class="">
<a href="{% link images/blog/gros-plan-certificats-medicaux/diagramme-televerses.png %}" rel="prettyPhoto[diagramme-televerses]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/diagramme-televerses.png %}" alt="Diagramme de séquence pour les certificats televersés." />
</a>
<figcaption>
Diagramme de séquence pour les certificats televersés.
</figcaption>
</figure>

Vous noterez que le praticien part d'un modèle de document texte déjà présent sur son ordinateur. Afin que tous les praticiens partagent les mêmes modèles de document, nous avons développé au sein de Filaé un référentiel de modèles. Maintenu à jour par les administrateurs de la plateforme, il garantit la disponibilité des documents dans leur plus récente version et donc la cohérence des certificats entre les praticiens.


```plantuml!
participant Ordinateur
participant RéférentielDeModèles
participant PriseEnCharge

Ordinateur->>RéférentielDeModèles: [Administrateur] Téléversement VV.odt
note right of RéférentielDeModèles: Modèle Texte
Ordinateur->>RéférentielDeModèles: [Administrateur] Téléversement AS.odt
note right of RéférentielDeModèles: Modèle Texte
Ordinateur->>RéférentielDeModèles: [Administrateur] Téléversement AVP.odt
note right of RéférentielDeModèles: Modèle Texte
Ordinateur->>RéférentielDeModèles: [Administrateur] Téléversement ASv2.odt
note right of RéférentielDeModèles: Modèle Texte
Ordinateur->>PriseEnCharge: [Praticien] Clic
PriseEnCharge->>CatalogueDeModèles: [Praticien] Clic VV.odt
CatalogueDeModèles->>Ordinateur: [Praticien] Téléchargement d'une copie vierge
note left of Ordinateur: Modèle Texte
```

<figure class="">
<a href="{% link images/blog/gros-plan-certificats-medicaux/diagramme-referentiel.png %}" rel="prettyPhoto[diagramme-referentiel]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/diagramme-referentiel.png %}" alt="Diagramme de séquence pour l'administration du référentiel de modèles de certificat." />
</a>
<figcaption>
Diagramme de séquence pour l'administration du référentiel de modèles de certificat.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-referentiel.png %}" rel="prettyPhoto[captures-televerses]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-referentiel.png %}" alt="Capture d'écran de l'administration du référentiel de modèles de certificat." />
</a>
<figcaption>
Capture d'écran de l'administration du référentiel de modèles de certificat.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-prise-en-charge.png %}" rel="prettyPhoto[captures-televerses]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-prise-en-charge.png %}" alt="Capture d'écran du menu de choix de modèle à la prise en charge." />
</a>
<figcaption>
Capture d'écran du menu de choix de modèle à la prise en charge.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-vierge.png %}" rel="prettyPhoto[captures-televerses]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-vierge.png %}" alt="Capture d'écran de la page de consultation vierge de tout certificat téléversé." />
</a>
<figcaption>
Capture d'écran de la page de consultation vierge de tout certificat téléversé.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-signe.png %}" rel="prettyPhoto[captures-televerses]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-signe.png %}" alt="Capture d'écran du certificat avec signature manuscrite numérisé et téléversé." />
</a>
<figcaption>
Capture d'écran du certificat avec signature manuscrite numérisé et téléversé.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-valide.png %}" rel="prettyPhoto[captures-televerses]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-valide.png %}" alt="Capture d'écran du certificat validé, scellé, prêt à être envoyé." />
</a>
<figcaption>
Capture d'écran du certificat validé, scellé, prêt à être envoyé.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-envoi.png %}" rel="prettyPhoto[captures-televerses]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-envoi.png %}" alt="Capture d'écran de la fenêtre modale d'envoi au service requérant." />
</a>
<figcaption>
Capture d'écran de la fenêtre modale d'envoi au service requérant.
</figcaption>
</figure>

Comme nous l'avons mentionné un peu plus tôt, il s'agit là d'un mode opératoire intermédiaire, permettant aux praticiens de saisir leurs certificats avec leurs modèles habituels, et de se servir de Filaé pour la collaboration avec les autres parties prenantes : secrétaire, <abbr title="Infirmier·e Diplomé·e d'État">IDE</abbr>, <abbr title="Officier de Police Judiciaire">OPJ</abbr>… Mais l'idéal serait quand-même que les praticiens n'aient pas besoin d'outil externe pour la rédaction de leurs certificats, et qu'ils puissent tout faire sans quitter Filaé.

## Les certificats embarqués

Grâce à une [bibliothèque externe](https://www.surveyjs.io/) intégrée à Filaé, cette possibilité est offerte au praticien, en complément des modèles de certificat téléversés. La conception des certificats demande un investissement certain, mais l'expérience du praticien est bien supérieure.

```plantuml!
Ordinateur->>RéférentielDeModèles: [Administrateur] Conception VV embarqué
note right of RéférentielDeModèles: Document JSON
Ordinateur->>PriseEnCharge: [Praticien] Clic
PriseEnCharge->>CatalogueDeModèles: [Praticien] Clic VV embarqué
note right of CatalogueDeModèles: Formulaire Web
CatalogueDeModèles->>CertificatAmorcé: [Praticien] Rédaction
note right of CertificatAmorcé: Formulaire Web
CertificatAmorcé->>CertificatAmorcé: [Praticien] Clic pour aperçu
note right of CertificatAmorcé: Document PDF
CertificatAmorcé->>CertificatValidé: [Praticien] Clic pour signature électronique
note right of CertificatValidé: Document PDF + Sceau
CertificatValidé->>OPJ: [Praticien] Clic
note right of OPJ: Document PDF + Sceau
CertificatValidé->>CertificatEnvoyé: [Automatique] Horodatage
note right of CertificatEnvoyé: Document PDF + Sceau
```

<figure class="">
<a href="{% link images/blog/gros-plan-certificats-medicaux/diagramme-embarques.png %}" rel="prettyPhoto[diagramme-embarques]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/diagramme-embarques.png %}" alt="Diagramme de séquence pour les certificats embarqués." />
</a>
<figcaption>
Diagramme de séquence pour les certificats embarqués.
</figcaption>
</figure>

Comme on le voit ici, le praticien n'a pas besoin de quitter Filaé entre le moment de la prise en charge et l'envoi à l'OPJ. C'est un gain évident d'efficacité, limitant les allers-retours entre logiciels, toujours risques de fausse manipulation.

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-embarque-redaction.png %}" rel="prettyPhoto[captures-embarque]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-embarque-redaction.png %}" alt="Capture d'écran de la rédaction d'un certificat embarqué." />
</a>
<figcaption>
Capture d'écran de la rédaction d'un certificat embarqué.
</figcaption>
</figure>

<figure class="offer-col">
<a href="{% link images/blog/gros-plan-certificats-medicaux/capture-embarque-valide.png %}" rel="prettyPhoto[captures-embarque]">
  <img src="{% link images/blog/gros-plan-certificats-medicaux/capture-embarque-valide.png %}" alt="Capture d'écran d'un certificat embarqué validé." />
</a>
<figcaption>
Capture d'écran d'un certificat embarqué validé.
</figcaption>
</figure>

Malheureusement, dans ce monde idéal, il y a un hic : la flèche `Clic pour signature électronique` n'a pas encore été développée dans Filaé. Le praticien doit donc imprimer la version PDF du certificat embarqué, y apposer sa signature manuscrite, numériser et téléverser ce certificat signé, et ainsi de suite, comme dans le premier schéma ! Ce n'est pas faute d'avoir cherché, mais pour l'instant, aucune solution de signature électronique ne trouve grâce à nos yeux.

Si vous souhaitez en savoir plus sur nos difficultés avec la signature électronique, [l'article suivant]({% post_url 2022-03-07-signature-electronique-via-e-cps %}) en détaille les tenants et aboutissants.
{: .follow-up }

</div>
