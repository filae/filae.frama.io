---
layout: post
title:  "Le Congrès International Francophone : la libération !"
categories: blog
---

Ophélie Ferrant-Azoulay, chef de service de l'<abbr title='Unité Médico-Judiciaire'>UMJ</abbr> des Yvelines, présentait Filaé lors du 52ème Congrès International Francophone de Médecine Légale. Découvrons ensemble ce qu'il s'est passé à Montpellier ce mercredi 22 septembre 2021.

## Filaé : le virage numérique

Cette première présentation publique était l'occasion de revenir sur la genèse du projet, son déroulement et bien sûr ses perspectives d'essaimage.

Plutôt que de vous assommer de texte, laissez-nous vous partager le jeu de diapositives réalisé par le Dr Ferrant-Azoulay pour ses pairs.

<figure>
<a href="{% link files/Filaé-CIFML-Montpellier-2021.pdf %}">
  <img src="{% link images/blog/filae-cifml-2021-cover.png %}" alt="Première diapositive de la présentation" />
</a>
<figcaption>Cliquez sur l'image pour télécharger le PDF complet.</figcaption>
</figure>

Les discussions sont allées bon train pendant et après la présentation. Le processus de dématérialisation de la procédure soulève de nombreuses interrogations pour lesquelles Filaé n'a pas encore toutes les réponses. Autant la fluidification des interactions au sein de l'unité est sujet bien outillé, autant la communication avec les instances extérieures reste un chantier à boucler. Nous avons tenté des choses, mais la question de la force probante (la signature électronique, pour faire un raccourci) reste en suspens.

Ces discussions engagées nous permettront peut-être de faire converger les pratiques et réduire les inégalités de services rendus à toutes les parties prenantes. À l'heure actuelle, chaque unité dépend des priorités budgétaires et techniques de son groupe hospitalier. La libération de Filaé est une solution offerte à la communauté des médecins légistes pour mutualiser l'effort et créer ensemble quelque chose de plus grand que la somme des parties.

## Filaé : le code disponible

Nous vous l'annoncions il y a quelques mois, Filaé était pensé depuis le début comme un logiciel libre, d'où son haut degré de personnalisation pour une solution ad-hoc. Mais le code n'était pas encore disponible. De discrètes allusions à l'<abbr title='Unité Médico-Judiciaire'>UMJ78</abbr> s'étaient glissées dans le code de ci de là. Quelques options de configuration plus tard, voici [Filaé prêt](https://framagit.org/filae/src) à être déployé dans n'importe quelle unité.

**Le code source n'est pas la seule contribution de valeur**. Idées, avis, problèmes, guides d'utilisation… [Entrez dans la danse](https://framagit.org/filae/src/-/issues) et appréciez la construction collaborative de biens communs offerte par le logiciel libre. Un beau chantier démarre prochainement avec l'équipe versaillaise, il va y avoir de l'animation ; n'hésitez pas à venir découvrir l'effervescence d'un développement agile. Et pourquoi pas y apporter votre touche personnelle.

Cette libération ouvre également la porte à la mise à disposition d'une installation de démonstration, que vous puissiez utiliser le logiciel avant de l'installer de votre côté. C'est une idée que nous avons depuis longtemps en tête, peut-être trouverons-nous l'opportunité de mettre cela en œuvre dans un avenir proche.

À très vite pour la suite de nos aventures. Entre temps, nos coordonnées sont toujours en pied-de-page, à toutes fins utiles 👋
