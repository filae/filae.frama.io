---
layout: post
title:  "FILAÉ, logiciel libre pour <abbr title='Unité Médico-Judiciaire'>UMJ</abbr>"
categories: blog
---

Ces dernières années ont été consacrées à une réalisation pleine de belles perspectives d'[essaimage](https://www.infopiiaf.fr/blog/2015/07/07/libre-et-agile-un-dessin-d-essaim.html). L'unité médico-judiciaire des Yvelines ([UMJ78](https://www.ch-versailles.fr/UMJ/5/5/43)) et [infoPiiaf](https://www.infopiiaf.fr) ont le plaisir de vous annoncer la libération prochaine du fruit de leur collaboration : FILAÉ.

## Du papier-crayon à l'application web

### Une idée longuement mûrie…

Depuis de nombreuses années, l'équipe de l'<abbr title='Unité Médico-Judiciaire des Yvelines'>UMJ78</abbr> imaginait de meilleures façons de collaborer au sein de leur service, sans pouvoir les mettre en œuvre jusqu'à présent, n'ayant pas trouvé de solution logicielle vraiment adaptée à leurs besoins. Ils ont donc envisagé le développement d'un logiciel :
1. **Spécifique**, en parfaite adéquation avec les exigences de leurs activités d'aujourd'hui, et évolutif, pour celles de demain ;
1. **Robuste et performant**, capable de gérer les milliers de rendez-vous et dossiers traités au fil des ans ;
1. **Fiable et sûr**, eu égard à la criticité des données manipulées, à caractère médical et judiciaire.

À l'époque, leurs modes de fonctionnement reposaient essentiellement sur :
* Des outils disparates (papier et crayons compris) qui généraient de nombreuses
**saisies multiples**, avec la perte de temps et les risques d'erreur inhérents ;
* Des données faiblement structurées et difficilement indexables telles que des
documents Word ou des tableaux Excel.

Ils souhaitaient un véritable outil de travail les aidant au quotidien à :
* **Coordonner** leurs activités, de la prise de rendez-vous à l'envoi et stockage des
dossiers ;
* **Parcourir** les dossiers et médias à des fins documentaires ou statistiques ;
* **Accueillir** de nouvelles personnes dans leur service, rapidement opérationnelles.

### Une réalisation en étroite collaboration.

La société infoPiiaf a été choisie pour les accompagner dans la réalisation de cet outil sur-mesure :
1. établissement des **lignes directrices** de l'application cible ;
2. développement des **fonctionnalités prioritaires** de ladite application.

Plutôt qu'un logiciel à installer sur tous les postes, leur solution reposait sur le développement d'une application Web :
* **Accessible simplement** grâce à un navigateur Internet (y compris en situation de mobilité), et toujours à jour sans action de leur part ;
* **Pérenne**, puisque indépendante du parc informatique du service, risques d'incompatibilité future extrêmement faible.

Pour bénéficier au plus tôt des apports de la solution logicielle, les méthodes agiles étaient idéales :
* **Incrémentales** : ajouter progressivement de nouvelles fonctionnalités dans un logiciel déjà utilisable au quotidien ;
* **Itératives** : améliorer ce qui existe pour s'adapter aux éléments nouveaux et aux retours du terrain.

La condition _sine qua non_ formulée par infoPiiaf de libérer ce logiciel, dans une démarche d'[essaimage](https://www.infopiiaf.fr/blog/2015/07/07/libre-et-agile-un-dessin-d-essaim.html), a rapidement trouvé un écho favorable. L'enthousiasme partagé à ce sujet a fini de convaincre toutes les parties prenantes que cette collaboration pourrait être **des plus fructueuses**… Et elle le fût !

### Des débuts prometteurs !

Nous ne nous attarderons pas dans ce billet sur la liste des fonctionnalités développées, une [page y est consacrée]({% link fonctionnalites/index.md %}). Nous souhaitons plutôt revenir sur les grandes lignes de sa réalisation.

Ce projet nous a permis de mettre en œuvre bien des bienfaits des méthodes agiles, avec des interactions fréquentes entre l'UMJ78 et infoPiiaf, une mise en production rapide des premières fonctionnalités et des déploiements au fil de l'eau des nouveautés, évolutions et corrections.

Nos efforts se sont concentrés en premier lieu sur le flux de consultations en antenne mobile, les médecins en déplacement tirant immédiatement profit d'informations instantanément disponibles et à jour. Le 6 septembre 2018, la version `1.0` était mise en production et entre les mains de tous.

Tout en prenant en compte les améliorations remontées par l'équipe sur le mode flux, nous avons concentré nos efforts sur les agendas partagés pour les consultations sur rendez-vous. Puis sur les consultations complémentaires… Puis sur le cycle de vie des certificats… À l'heure où ces lignes sont écrites, FILAÉ est en version `1.19.3`, soit près d'une mise en production par mois ; avant que la situation sanitaire ne ralentisse considérablement notre progression.

FILAÉ est utilisé quotidiennement depuis septembre 2018, fluidifiant la communication entre le personnel médical et judiciaire. Par extension, c'est le parcours de milliers de patients par an qui s'en trouve facilité. Grâce à la libération prochaine de ce logiciel, nous espérons que cela pourra durer encore longtemps !

## Encore du chemin à parcourir… Ensemble ?

Ce projet va maintenant être lâché dans la nature. Ce qui lui arrivera dépend en partie de l'effervescence de la communauté que nous tenterons, conjointement, d'amorcer. Le secteur hospitalier se prête tout particulièrement à ce mode de fonctionnement vertueux qu'est l'[essaimage](https://www.infopiiaf.fr/blog/2015/07/07/libre-et-agile-un-dessin-d-essaim.html). À nous maintenant de susciter l'intérêt des homologues de l'<abbr title='Unité Médico-Judiciaire des Yvelines'>UMJ78</abbr> pour mutualiser les énergies disponibles !

> Tout seul, on va plus vite. Ensemble, on va plus loin.

Si vous souhaitez en savoir plus, voire contribuer à cette belle aventure, n'hésitez pas à [nous contacter <i class="fa fa-arrow-circle-down"></i>](#contact-us).
