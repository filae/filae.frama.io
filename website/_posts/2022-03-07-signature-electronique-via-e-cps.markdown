---
layout: post
title:  "Signature électronique via e-CPS"
categories: blog
---
Après en avoir appris plus sur [le cycle de vie des certificats médicaux]({% post_url 2022-03-04-gros-plan-certificats-medicaux %}), et découvert notre lacune concernant la signature électronique, détaillons précisément ce qui nous bloque.

Nous développons le logiciel libre Filaé pour le compte de l'Unité Médico-Judiciaire du Centre Hospitalier de Versailles. Les UMJ ont pour mission de réaliser des examens médicaux sur réquisition judiciaire à la demande des autorités (parquet, police, gendarmerie). Ainsi, les certificats médicaux produits sont à la fois des données de santé à caractère personnel et des données judiciaires. Tout au long de l'année, l'équipe d'une dizaine de médecins rédige quelques milliers de certificats médicaux, qu'ils doivent pour le moment signer sur papier et transmettre par voie postale.

## Filaé le bec dans l'eau

Filaé leur permet aujourd'hui de gérer le cycle de vie de ces certificats médicaux, de la prise de rendez-vous à leur stockage, mais il manque l'étape de signature électronique pour permettre une dématérialisation totale. Il s'agit donc, si vous nous passez l'expression, d'une auto-signature (je signe ce que je produis) devant avoir la même valeur légale qu'une signature manuscrite, dans le cadre d'une procédure médicale et judiciaire.

Cette signature doit pouvoir se faire sans quitter son navigateur Internet : le médecin rédige son certificat médical dans l'interface web de Filaé, l'application en produit une version PDF qui doit être signée via une [API](https://fr.wikipedia.org/wiki/Interface_de_programmation) de signature électronique. Ce PDF signé est alors récupéré par Filaé qui le stocke et le transmet à qui de droit.

Entre alors en lice le [référentiel force probante des documents de santé](https://esante.gouv.fr/webinaires/introduction-au-referentiel-force-probante-des-documents-de-sante). Ce référentiel nous indique que nous avons ici affaire au type de signature de plus haut niveau, dite signature électronique qualifiée dans le référentiel européen eIDAS. L'<abbr title="Agence du Numérique en Santé">ANS</abbr> indique bien [page 7](https://esante.gouv.fr/espace_documentation/referentiel-force-probante/annexe-3-mecanismes-de-securite-mettre-en-oeuvre-dans-le-cadre-de-la-production-de-documents-nativement-numeriques/actual) que :

> La signature électronique qualifiée (SEQ) est la seule à disposer de l’équivalence juridique avec la signature manuscrite.

Or, c'est là où le bât blesse : très peu d'acteurs proposent ce niveau de signature très contraignant. Il nécessite la création de comptes-utilisateurs chez cet opérateur certifié, et la vérification régulière de l'identité par une entrevue (physique ou virtuelle) de contrôle d'identité. Cela représente une charge de travail supplémentaire pour le personnel administratif de l'hôpital, et un coût supplémentaire non négligeable, ces contrôles humains n'étant évidemment pas gratuits.

Quatre ans de travail pour échouer si prêt du but ? Pas si sûr…

## La <abbr title="Carte de Professionnel de Santé dématérialisée">e-CPS</abbr> à la rescousse

La <abbr title="Carte de Professionnel de Santé">CPS</abbr> est une carte à puce délivrée au personnel de santé qui permet de garantir son identité via un lecteur de carte et un code secret. Connectée à un ordinateur, elle permet d'identifier de manière univoque qui est derrière l'écran. La <abbr title="Carte de Professionnel de Santé dématérialisée">[e-CPS](https://esante.gouv.fr/offres-services/e-cps)</abbr> est son pendant 100% numérique et mobile, ne nécessitant pas de lecteur de carte. À l'instar de l'authentification forte que vous connaissez avec les mécanismes de paiement en ligne avec code à usage unique par SMS par exemple, le personnel de santé peut s'authentifier avec son application mobile e-CPS et le portail [Pro Santé Connect](https://esante.gouv.fr/offres-services/referentiel-pro-sante-connect). Plus pratique qu'un lecteur de carte branché à une tablette quand on est dans une cellule de garde à vue ou au milieu de la fôret de Rambouillet, n'est-ce pas ?

Il se trouve qu'à chaque CPS est associé un certificat logiciel personnel permettant, notamment, de signer électroniquement des documents, et répondant au doux nom de certificat `PRO_SIGN`. Ce certificat, grâce à la garantie d'identité fournie par la CPS, nous permettrait d'offrir une signature qualifiée sans surcharge de travail pour le personnel de l'établissement de santé. Ni une ni deux, nous envisageons donc d'utiliser l'authentification par e-CPS pour récupérer le certificat `PRO_SIGN` du médecin à sa première connexion et ainsi signer électroniquement les documents PDF qu'il produira lors de ses consultations.

Malheureusement, impossible de trouver les API de récupération des certificats logiciels. Est-ce tout simplement réalisable ? Nous aimerions explorer cette solution de signature grâce à l'infrastructure proposée par l'ANS, mais à l'heure où j'écris ces lignes, nous manquons d'informations sur le _comment_. Nous leur avons demandé plusieurs fois, sans succès pour le moment.

## Imagineriez-vous…

Un service Web souverain permettant à tout le personnel de santé de signer ses documents avec le plus haut niveau de garantie à l'échelle européenne ? Un service Web libre, contributif, mutualisé, qui éviterait de transférer à des solutions privées tous ces documents de santé à caractère personnel ?

Nous oui. Malheureusement, aujourd'hui, nous n'arrivons pas à trouver dans tout le [catalogue d'offres et produits](https://esante.gouv.fr/offres-services) de l'ANS la combinaison gagnante…

Si vous avez des pistes, nos coordonnées sont [en bas de page](#contact-us) !
{: .follow-up }

09/03/2022 : après une nouvelle salve de courriels vers l'ANS, nous avons eu un contact prometteur avec l'équipe Pro Santé Connect… À suivre !
{: .update }

28/07/2023 : tiens, nous avons oublié de publier ici les conclusions de nos discussions avec l'ANS il y a plus d'un an. Vous pouvez en prendre connaissance dans le [ticket idoine](https://framagit.org/filae/src/-/issues/47). N'hésitez pas à y apporter vos contributions.
{: .update }
