---
layout: post
title:  "Gros plan sur les agendas partagés"
categories: blog
---

Lorsque l'on passe sa journée à changer de contexte, que l'on soit médecin, secrétaire ou encore infirmier, notre outil de travail doit nous aider à y voir plus clair. Où en suis-je ? Que me reste-t-il à faire ? Qu'est-ce qui m'attend maintenant ? Tel patient est-il encore là ? Découvrons ce que sait faire Filaé dans ce domaine.

## Une image vaut mille mots

<figure>
<a href="{% link images/blog/gros-plan-agenda.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/blog/gros-plan-agenda.png %}" alt="Condensé des aides visuelles fournies par Filaé" />
</a>
</figure>

Il y en a des choses à dire sur cette image, alors comme elle, tentons d'être synthétiques :

1. Ces deux colonnes concernent la journée du vendredi 07 mai ;
1. Les deux horloges indiquent que la journée est concernée par un planning configuré avec deux tranches horaires de garde (matin et après-midi par exemple)
    * La première tranche horaire est assurée par deux intervenants ;
    * Personne n'est affecté à la deuxième partie de journée encore ;
    * Il y a donc pour l'instant au maximum deux intervenants en même temps. La journée est alors découpée en deux colonnes, deux pistes de rendez-vous ;
1. Chaque rendez-vous a une couleur de fond associée au motif de consultation, repris entre crochet avant les nom et prénom du patient : violences volontaires, âge osseux, maltraitance, accident de la voie publique, etc.
1. Le petit personnage devant "RELLA Cindy" indique qu'il s'agit d'un mineur ;
1. Le "R" au-dessus de "RELLA Cindy" indique que la réquisition a été reçu ;
1. Le cartouche sur fond clair en haut à droite permet de suivre le parcours-patient, voyons si vous arrivez à déduire des explications ci-dessous son fonctionnement :
    * Anna d'Arendelle a déjà été prise en charge par le médecin et a accepté deux consultations complémentaires, une avec un juriste, l'autre avec la psychologue. D'ailleurs, son rendez-vous avec cette dernière a déjà commencé, elle devra encore voir le juriste ensuite ;
    * Cindy Rella a vu le médecin et doit maintenant être prise en charge par la psychologue ;
    * Oliver Twist, lui, est actuellement en consultation avec le médecin ;
    * Robert André Raimbourg ne s'est pas encore présenté à l'accueil.
1. Pour terminer, des pictogrammes suivent l'identité du patient une fois la prise en charge débutée :
    * L'icône "document" indique que le certificat est validé et prêt à être envoyé au service requérent ;
    * L'icône "enveloppe" indique que le certificat a bien été envoyé.

Rassurez-vous, nul besoin de se souvenir de tout ça par cœur. Laisser sa souris sur la plupart de ces éléments affiche une petite bulle de description.

<figure>
<a href="{% link images/blog/gros-plan-agenda-tooltip.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/blog/gros-plan-agenda-tooltip.png %}" alt="Condensé des aides visuelles fournies par Filaé" />
</a>
</figure>

## Plutôt aidant dans le fond

Si vous regardez bien cette dernière capture d'écran, vous remarquerez que les colonnes ont différentes couleurs de fond également :

1. le jaune pâle (en haut à gauche) met en exergue la date d'aujourd'hui ;
1. le vert pastel (en bas à gauche) indique un créneau de garde avec au moins un intervenant affecté ;
1. le gris (en bas à droite) signale un créneau de garde où personne n'est encore planifié ;
1. le blanc (en haut à droite) marque tout simplement l'absence d'information significative à présenter.

Ainsi, ces agendas partagés sont conçus par faciliter bien évidemment le déroulé d'une journée de travail, mais également l'organisation à plus long terme de l'équipe.

Le compromis entre synthèse et richesse d'information n'est pas toujours évident à trouver. L'agenda a heureusement été construit petit à petit, au fur et à mesure du développement des nouvelles fonctionnalités. Cela nous a permis d'expérimenter, de corriger, d'ajuster, avant d'arriver, grâce aux retours du terrain, au consensus présenté ici.

Retrouvez-nous sur ce blog très rapidement pour un gros plan sur le mode de consultation en flux, plus adapté au travail en antenne mobile.
