---
layout: post
title:  "Gros plan sur les flux de consultations"
categories: blog
---

Il est minuit passé, vous montez dans votre voiture après une consultation en commissariat pour prolongation de garde à vue. Comment savez-vous où est votre prochaine étape ? Vous rappelez le standard de l'hôpital pour la douzième fois depuis le début de la soirée ? Vous consultez un calepin où vous avez noté il y a deux heures les derniers appels reçus ? Ou vous sortez votre tablette et consultez une application vous offrant toutes les informations nécessaires à une prise de décision éclairée à la lumière de données toujours à jour ?

## Une image vaut mille mots

<figure>
<a href="{% link images/blog/gros-plan-flux.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/blog/gros-plan-flux.png %}" alt="Condensé des aides visuelles fournies par Filaé pour les flux de consultations" />
</a>
</figure>

Il y en a des choses à dire sur cette image, alors comme elle, tentons d'être synthétiques.

Toutes les nouvelles demandes de consultation tombent dans la bannette "À transmettre" où l'on peut voir :

1. L'identifiant ONML généré automatiquement par Filaé (en attendant Medlé) ;
1. Le sablier comme indicateur visuel d'une consultation en attente de transmission à un médecin ;
1. Une bande de couleur indiquant le secteur géographique du service requérant ;
1. Le type de consultation ;
1. L'heure d'appel ;
1. Le service requérant, de la couleur de son secteur géographique. À noter que les consultations sont affichées triées par secteur et service requérant, ce qui permet une première approximation de la proximité géographique des demandes ;
1. Les informations du patient ;
1. Le trombone pour accéder aux fichiers attachés, affichant d'emblée leur nombre ;
1. L'horloge pour afficher l'historique de la consultation ;
1. Les boutons d'action rapide pour assigner la consultation à un médecin, annuler la demande ou modifier les informations renseignées.

Une fois assignée à un médecin, une consultation passe dans la bannette "En cours" et se voit attribuée deux nouvelles actions possibles :

1. Téléverser des certificats signés pour envoi par courriel et stockage ;
1. Marquer la consultation comme validée.

Qu'elles soient validées ou annulées, les consultations n'apparaissent plus dans cet écran au bout de douze heures. Elles peuvent être alors retrouvées dans les archives. Une batterie de critères de recherche permet de retrouver une consultation en un tournemain.

<figure>
<a href="{% link images/blog/gros-plan-flux-archives.png %}" rel="prettyPhoto[captures]">
  <img src="{% link images/blog/gros-plan-flux-archives.png %}" alt="Archive des flux de consultations" />
</a>
</figure>

## En situation de mobilité, aussi et surtout

Ces écrans ont beau être riches en infos et en actions disponibles, ils n'en sont pas moins utilisables sur les écrans tactiles de vos périphériques mobiles, téléphones ou tablettes. Une simple connexion 3G permet de récupérer les dernières informations, faire le point du bout des doigts et poursuivre votre périple. Bonne route !
