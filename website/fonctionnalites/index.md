---
layout: page
title:  "Fonctionnalités"
nav: header
order: 2
---

FILAÉ a pour mission d'aider les unités médico-judiciaires à remplir la leur : réaliser des examens médicaux sur réquisition judiciaire à la demande des autorités (parquet, police, gendarmerie). Entre consultations programmées dans vos locaux (victimes de violences volontaires, d'agressions sexuelles, de maltraitances, etc.), visites des personnes gardées à vue dans les commissariats du département et les levées de corps, la cohérence et l'intégrité des communications entre le personnel médical et les instances requérantes sont des enjeux cruciaux.

## Problématique

Historiquement, le quotidien de l'UMJ78 reposait essentiellement sur des outils disparates (papier et crayons compris) générant de nombreuses saisies multiples, avec la perte de temps et les risques d'erreur inhérents, et des données faiblement structurées et difficilement indexables telles que des documents Word ou des tableaux Excel.

Différents [contributeurs]({% link contributeurs/index.md %}) ont collaboré au développement d'un véritable outil de travail permettant de coordonner leurs activités, de la prise de rendez-vous à l'envoi et stockage des certificats médicaux, de parcourir les dossiers et médias à des fins documentaires ou statistiques, et d'accueillir de nouvelles personnes dans leur service, rapidement opérationnelles.

## Solution réalisée

<div class="commitment">
<div class="commitment-col commitment-id">
<i class="fa fa-calendar"></i>
<h3>Prise de rendez-vous.</h3>
</div>
<div class="commitment-col commitment-bottom" markdown="block">
#### Calendriers de rendez-vous

* Définition de multiples calendriers avec gestion fine des autorisations d'accès selon le rôle de l'utilisateur ;
* Catégorisation multi-niveaux, pour distinguer aisément le motif de consultation dans le calendrier et rechercher dans le passé ;
* Configuration de durées recommandées par catégorie de rendez-vous.
</div>
<div class="commitment-col commitment-bottom" markdown="block">
#### Flux de consultation

* Saisie des sollicitations au fil de l'eau à destination du personnel en antenne mobile, listées selon leur avancement (à transmettre, en cours, etc.) ;
* Configuration des secteurs géographiques et tri par secteur et service requérant dans la liste pour grouper les déplacements ;
* Présentation conçue pour un usage en situation de mobilité sur petits écrans.
</div>
</div>

<div class="commitment">
<div class="commitment-col commitment-id">
<i class="fa fa-pencil-square-o"></i>
<h3>Certificats médicaux.</h3>
</div>
<div class="commitment-col commitment-bottom" markdown="block">
#### Bibliothèque de modèles

* Référentiel partagé de modèles de certificats, pour un stockage centralisé et toujours à jour ;
* Téléchargement disponible directement depuis le rendez-vous dans l'agenda ou la consultation en flux ;
* Restriction d'accès en fonction des rôles des utilisateurs.
</div>
<div class="commitment-col commitment-bottom" markdown="block">
#### Gestion du cycle de vie

* Du brouillon à l'envoi par courriel en passant par le stockage en PDF, l'avancement du certificat médical est visible en un coup d'œil ;
* Stockage sécurisé de signature personnelle numérisée pour une disponibilité en toute circonstance ;
* Contrôle d'intégrité du document PDF disponible sur demande.
</div>
</div>

<div class="commitment">
<div class="commitment-col commitment-id">
<i class="fa fa-lightbulb-o"></i>
<h3>Idées à concrétiser…</h3>
</div>
<div class="commitment-col commitment-bottom" markdown="block">
#### Certificats embarqués

* Conception des modèles de certificat dans un éditeur de formulaires en ligne ;
* Rédaction des certificats directement dans l'application, sans détour par un logiciel externe ;
* Intégration au cœur d'un gabarit commun à tous les certificats pour un rendu homogène.
</div>
<div class="commitment-col commitment-bottom" markdown="block">
#### Confort d'utilisation

* Répertoire des patients et <abbr title="Officiers de Police Judiciaire">OPJ</abbr> pour une prise de rendez-vous accélérée ;
* Fiche-patient pour résumer les informations personnelles et toutes les consultations le concernant ;
* Utilisation hors-ligne, notamment en zones blanches (cellules, forêts, etc.).
</div>
</div>

## Perspectives

Convaincus dès nos premières discussions par la [démarche d'essaimage](https://www.infopiiaf.fr/blog/2015/07/07/libre-et-agile-un-dessin-d-essaim.html), l'<abbr title="Unité Médico-Judiciaire des Yvelines">UMJ78</abbr> a eu l'audace et l'énergie d'investir dans le développement des premières fonctionnalités de FILAÉ. Il serait maintenant extrêmement vertueux que d'autres <abbr title='unités médico-judiciaires'>UMJ</abbr> (ou services similaires) se joignent à eux pour mutualiser l'effort et proposer de nouvelles versions de ce [logiciel (bientôt) libre](https://www.infopiiaf.fr/blog/2015/05/11/logiciel-libre.html) qui profiteraient à tous.

> Tout seul, on va plus vite. Ensemble, on va plus loin.

Si vous souhaitez en savoir plus, voire contribuer à cette belle aventure, n'hésitez pas à [nous contacter <i class="fa fa-arrow-circle-down"></i>](#contact-us).
