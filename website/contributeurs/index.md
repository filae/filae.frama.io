---
layout: page
title: Contributeurs
nav: header
order: 3
---

<div class="person legal">
<div class="person-col person-id">
	<img src="{% link images/ch-versailles_logo.jpg %}" />
	<h2>UMJ78 - CH Versailles.</h2>
	<p markdown="span">
	[<i class="fa fa-globe"></i>](https://www.ch-versailles.fr/UMJ/5/5/43 "Site web UMJ78")
	</p>
</div>
<div class="person-col person-curriculum" markdown="block">
Pendant de nombreuses années, l’équipe de l’UMJ78 imaginait de **meilleures façons de collaborer** au sein de leur service, sans pouvoir mettre la main sur une solution logicielle vraiment adaptée à leurs besoins.
</div>
<div class="person-col person-intentions" markdown="block">
Ils ont donc franchi le pas : amorcer le développement d’un logiciel **spécifique**, **robuste et performant**, **fiable et sûr**. Et y consacrer quantité d'heures en plus de leur obligations quotidiennes.
</div>
<div class="person-col person-interests" markdown="block">
Après avoir eu l’audace et l’énergie d’investir dans les premières fonctionnalités de FILAÉ, l'équipe cherche à fédérer les UMJ autour du projet, pour construire ensemble quelque chose de plus grand que la somme des parties.
</div>
</div>

<div class="person legal">
<div class="person-col person-id">
	<img src="{% link images/infoPiiaf_logo.png %}" />
	<h2>infoPiiaf.</h2>
	<p markdown="span">
	[<i class="fa fa-globe"></i>](https://www.infopiiaf.fr "Site web infoPiiaf")
	[<i class="fa fa-gitlab"></i>](https://framagit.org/infopiiaf "Compte Framagit infoPiiaf")
	</p>
</div>
<div class="person-col person-curriculum" markdown="block">
Espèce en voie de développement, issue du croisement d'un [moineau](https://fr.wikipedia.org/wiki/Moineau_friquet), d'un [colibri](http://www.colibris-lemouvement.org/colibris/colibris-et-la-legende) et de [logiciel](http://www.april.org). Elle tire donc son nom de ce mélange singulier de volatiles et de technologie.
</div>
<div class="person-col person-intentions" markdown="block">
Particulièrement attachée à la liberté et au partage, elle cherchera à proposer ses services et conseils aux forces locales qui tentent de bâtir des nids plus accueillants pour tous.
</div>
<div class="person-col person-interests" markdown="block">
Prestataire historique (mais sans exclusivité), infoPiiaf poursuit l'aventure pour concrétiser avec FILAÉ ses aspirations profondes : outiller les initiatives locales et solidaires, en ligne avec notre vision affichée d’un internet libre et respectueux de l’autonomie de ses utilisateurs.
</div>
</div>

<div class="person legal">
<div class="person-col person-id">
	<img src="{% link images/openmoji_E246_black.png %}" />
	<h2>Vous ?</h2>
</div>
<div class="person-col person-curriculum" markdown="block">
FILAÉ est une solution logicielle libre. Son avenir dépend de la communauté qui l'anime. Utilisateurs, commanditaires, développeurs, auditeurs, tout le monde peut apporter sa pierre à l'édifice.
</div>
<div class="person-col person-intentions" markdown="block">
Du retour-terrain au financement de nouvelles fonctionnalités en passant par l'écriture du manuel utilisateur, toutes les contributions ont de la valeur. Partagez-nous vos réflexions et construisons la suite ensemble.
</div>
<div class="person-col person-interests" markdown="block">
Un peu de votre temps pour tester les nouveautés ? Un peu de budget pour financer la poursuite des développements ? Un peu de bouche à oreilles pour faire connaître FILAÉ ? Les petits ruisseaux font les grandes rivières.
</div>
</div>

[Votre premier message](#contact-us) sera peut-être la source d'une belle aventure.
{: .follow-up }
