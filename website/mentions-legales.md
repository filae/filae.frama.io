---
layout: page
title: Mentions légales
nav: footer
order: 3
---

FILAÉ est un logiciel libre porté par l'unité médico-judiciaire des Yvelines du centre hospitalier de Versailles.

## Coordonnées

<dl>
	<dt>Postales</dt>
	<dd>
		UMJ78<br>
		50, rue Berthier<br>
		78000 Versailles
	</dd>
	<dt>Électroniques</dt>
	<dd markdown="span">[En bas de page](#contact-us)</dd>
</dl>

## Informations légales

<dl>
	<dt>Directeur de la publication</dt>
	<dd>David Friquet</dd>
	<dt>Hébergeur</dt>
	<dd markdown="span">[Framagit](http://www.framagit.org)</dd>
</dl>

## Vie privée

Le présent site utilise [Matomo](https://matomo.org/) pour la mesure d’audience et respecte les [recommandations de la CNIL](http://www.cnil.fr/vos-obligations/sites-web-cookies-et-autres-traceurs/outils-et-codes-sources/la-mesure-daudience/), notamment l’anonymisation des données et la géolocalisation au niveau de la ville.

<iframe id="piwik-optout" src="https://stats.infopiiaf.fr/index.php?module=CoreAdminHome&action=optOut&language=fr"></iframe>
